e3-cfhvms/cfhvms/cfhvmsApp/Db
==============================
Database template and substitution files for the ESS Site-specific EPICS module: cfhvms (*Convential Facilities High-Voltage Monitoring System*)

## Template files
The template files defines sets of records and are used as the basic building blocks for the database.
- **HVInSlot.template**: Defines records for the PV's associated with an ingoing slot in a high voltage substation
- **HVOutSlot.template**: Defines records for the PV's associated with an outgoing slot in a high voltage substation
- **HVTransSlot.template**: Defines records for the PV's associated with a transmission slot in a high voltage substation
- **HVSubstationTrip.template**: Defines a calc record to aggregate trip information for slots in a high volateg substation
- **LVStation.template**: Defines recors (currently just one) for a low volatge swichgear

## Substitution files
The substitution files combines sets of template files to generate complete record sets for different classes of high voltage substations.
- **HVSubstation11.substitutions**: Creates record set for 11 slot high voltage substation (used by primary substations J1 and J2 in H05)
- **HVSubstation12.substitutions**: Creates record set for 12 slot high voltage substation (only used by J26 in H01 Cub)
- **HVSubstation19.substitutions**: Creates record set for 19 slot high voltage substation (used by distribution substations J3 and J4 in H06)
- **HVSubstation4.substitutions**: Creates record set for 4 slot high voltage substation (used by substations J30 in B02 Campus)
- **HVSubstation5.substitutions**: Creates record set for 5 slot high voltage substation (used by substations L10 in G04 Cryo)
- **HVSubstation5b.substitutions**: Creates record set for 5 slot backup high voltage substation (used by substations J5 in H06)
- **HVSubstation6.substitutions**: Creates record set for 6 slot backup high voltage substation (used by substations N1 and N2 in H06)
- **HVSubstation7.substitutions**: Creates record set for 7 slot high voltage substation (used by substations J17 in G04 Cryo)
- **HVSubstation8.substitutions**: Creates record set for 8 slot high voltage substation (used by all substations G02 accelerator)
