from epics import PV
from time import sleep
from run_iocsh import IOC
from os import environ
from datetime import datetime
import os
import pytest
import subprocess
import resource
import time
import signal


class opcuaTestHarness:
    def __init__(self):

        # Get values from the environment
        self.EPICS_BASE = environ.get("EPICS_BASE")
        self.REQUIRE_VERSION = environ.get("E3_REQUIRE_VERSION")
        self.MOD_VERSION = environ.get("E3_MODULE_VERSION")
        if self.MOD_VERSION is None:
            self.MOD_VERSION = "0.8.0"
        self.TEMP_CELL_PATH = environ.get("TEMP_CELL_PATH")
        if self.TEMP_CELL_PATH is None:
            self.TEMP_CELL_PATH = "cellMods"

        # run-iocsh parameters
        self.IOCSH_PATH = (
            f"{self.EPICS_BASE}/require/{self.REQUIRE_VERSION}/bin/iocsh.bash"
        )

        self.TestArgs = [
            "-l",
            self.TEMP_CELL_PATH,
            "-r",
            f"opcua,{self.MOD_VERSION}",
        ]

        self.cmd = "st_test.cmd"

        # Default IOC
        self.IOC = self.get_ioc()

        # timeout value in seconds for pvput/pvget calls
        self.timeout = 5
        self.putTimeout = self.timeout
        self.getTimeout = self.timeout

        # test sleep time ins seconds
        self.sleepTime = 3

        # Test server
        self.testServer = "./server/cfhvmsTestServer"
        self.isServerRunning = False
        self.serverURI = "opc.tcp://localhost.localdomain:4840"
        self.serverFakeTime = "2019-05-02 09:22:52"

        # Message catalog
        self.connectMsg = (
            "OPC UA session OPC1: connection status changed"
            + " from Connected to Disconnected"
        )
        self.reconnectMsg = (
            "OPC UA session OPC1: connection status changed"
            + " from ConnectionErrorApiReconnect to NewSessionCreated"
        )
        self.reconnectMsg1 = (
            "OPC UA session OPC1: connection status changed"
            + " from NewSessionCreated to Connected"
        )
        self.noConnectMsg = (
            "OPC UA session OPC1: connection status changed"
            + " from Disconnected to ConnectionErrorApiReconnect"
        )

        self.badNodeIdMsg = "item ns=2;s=Sim.BadVarName : BadNodeIdUnknown"

        # Server variables
        self.serverVars = [
            "open62541",
            "open62541 OPC UA Server",
            "1.2.0-29-g875d33a9",
        ]

    def get_ioc(self):
        return IOC(
            *self.TestArgs,
            self.cmd,
            ioc_executable=self.IOCSH_PATH,
            timeout=20.0,
        )

    def start_server(self, withPIPE=False):
        if withPIPE:
            self.serverProc = subprocess.Popen(
                self.testServer,
                shell=False,
                stdout=subprocess.PIPE,
            )
        else:
            self.serverProc = subprocess.Popen(
                self.testServer,
                shell=False,
            )

        print("\nOpening server with pid = %s" % self.serverProc.pid)
        retryCount = 0
        while (not self.isServerRunning) and retryCount < 5:
            # Poll server to see if it is running
            self.is_server_running()
            retryCount = retryCount + 1
            sleep(1)

        assert retryCount < 5, "Unable to start server"

    def start_server_with_faketime(self):
        self.serverProc = subprocess.Popen(
            "faketime -f '%s' %s" % (self.serverFakeTime, self.testServer),
            shell=True,
            preexec_fn=os.setsid,
        )

        print("\nOpening server with pid = %s" % self.serverProc.pid)
        retryCount = 0
        while (not self.isServerRunning) and retryCount < 5:
            # Poll server to see if it is running
            self.is_server_running()
            retryCount = retryCount + 1
            sleep(1)

        assert retryCount < 5, "Unable to start server"

    def stop_server_group(self):
        # Get the process group ID for the spawned shell,
        # and send terminate signal
        print("\nClosing server group with pgid = %s" % self.serverProc.pid)
        os.killpg(os.getpgid(self.serverProc.pid), signal.SIGTERM)
        # Update if server is running
        self.is_server_running()

    def stop_server(self):
        print("\nClosing server with pid = %s" % self.serverProc.pid)
        # Send terminate signal
        self.serverProc.terminate()
        # Wait for processes to terminate.
        self.serverProc.wait(timeout=20)
        # Update if server is running
        self.is_server_running()

    def is_server_running(self):
        from opcua import Client

        c = Client(self.serverURI)
        try:
            # Connect to server
            c.connect()
            # NS0|2259 is the server state variable
            # 0 -- Running
            var = c.get_node("ns=0;i=2259")
            val = var.get_data_value()
            self.isServerRunning = val.StatusCode.is_good()
            # Disconnect from server
            c.disconnect()

        except Exception:
            self.isServerRunning = False


# Standard test fixture
@pytest.fixture(scope="function")
def test_inst():
    """
    Instantiate test harness, start the server,
    yield the harness handle to the test,
    close the server on test end / failure
    """
    # Create handle to Test Harness
    test_inst = opcuaTestHarness()
    # Poll to see if the server is running
    test_inst.is_server_running()
    assert not (
        test_inst.isServerRunning
    ), "An instance of the OPC-UA test server is already running"
    # Start server
    test_inst.start_server()
    
    # Check that server is running
    test_inst.is_server_running()
    assert test_inst.isServerRunning

    # Drop to test
    yield test_inst
    # Shutdown server by sending terminate signal
    test_inst.stop_server()
    # Check server is stopped
    assert not test_inst.isServerRunning


# test fixture for use with timezone server
@pytest.fixture(scope="function")
def test_inst_TZ():
    """
    Instantiate test harness, start the server,
    yield the harness handle to the test,
    close the server on test end / failure
    """
    # Create handle to Test Harness
    test_inst_TZ = opcuaTestHarness()
    # Poll to see if the server is running
    test_inst_TZ.is_server_running()
    assert not (
        test_inst_TZ.isServerRunning
    ), "An instance of the OPC-UA test server is already running"
    # Start server
    test_inst_TZ.start_server_with_faketime()
    # Drop to test
    yield test_inst_TZ
    # Shutdown server by sending terminate signal
    test_inst_TZ.stop_server_group()
    # Check server is stopped
    assert not test_inst_TZ.isServerRunning


class TestUnitTests:

    def test_UH_001(self, test_inst):
        """
        Start the server, start the IOC. 
        Read current and voltage from T1 and T2
        """
        
        ioc = test_inst.IOC

        ioc.start()
        assert ioc.is_running()
        sleep(test_inst.sleepTime)
      
        ## Read voltage on T1
        pvName = "Pwr-J1U01:CnPw-Q-001:Voltage-RB"
        pv = PV(pvName)
        pvalue = pv.get(timeout=test_inst.getTimeout)
        assert pvalue == 1.01216
        assert pv.severity == 0

        ## Read current L1 on T1
        pvName = "Pwr-J1U01:CnPw-Q-001:CurrentL1-RB"
        pv = PV(pvName)
        pvalue = pv.get(timeout=test_inst.getTimeout)
        assert pvalue == 1.011
        assert pv.severity == 0

        ## Read current L2 on T1
        pvName = "Pwr-J1U01:CnPw-Q-001:CurrentL2-RB"
        pv = PV(pvName)
        pvalue = pv.get(timeout=test_inst.getTimeout)
        assert pvalue == 1.0111
        assert pv.severity == 0

        ## Read current L3 on T1
        pvName = "Pwr-J1U01:CnPw-Q-001:CurrentL3-RB"
        pv = PV(pvName)
        pvalue = pv.get(timeout=test_inst.getTimeout)
        assert pvalue == 1.0112
        assert pv.severity == 0

        ## Read voltage on T2
        pvName = "Pwr-J2U01:CnPw-Q-001:Voltage-RB"
        pv = PV(pvName)
        pvalue = pv.get(timeout=test_inst.getTimeout)
        assert pvalue == 2.1216
        assert pv.severity == 0

        ## Read current L1 on T2
        pvName = "Pwr-J2U01:CnPw-Q-001:CurrentL1-RB"
        pv = PV(pvName)
        pvalue = pv.get(timeout=test_inst.getTimeout)
        assert pvalue == 2.011
        assert pv.severity == 0

        ## Read current L2 on T2
        pvName = "Pwr-J2U01:CnPw-Q-001:CurrentL2-RB"
        pv = PV(pvName)
        pvalue = pv.get(timeout=test_inst.getTimeout)
        assert pvalue == 2.0111
        assert pv.severity == 0

        ## Read current L3 on T2
        pvName = "Pwr-J2U01:CnPw-Q-001:CurrentL3-RB"
        pv = PV(pvName)
        pvalue = pv.get(timeout=test_inst.getTimeout)
        assert pvalue == 2.0112
        assert pv.severity == 0

        ## Read current L3 on J7 (should be zero and alarm severity INVALID since it is not implemented on server)
        pvName = "Pwr-J7U01:CnPw-Q-001:CurrentL3-RB"
        pv = PV(pvName)
        pvalue = pv.get(timeout=test_inst.getTimeout)
        assert pvalue == 0
        assert pv.severity == 3

        ioc.exit()
        assert not ioc.is_running()

    def test_UH_002(self, test_inst):
        """
        Start the server, start the IOC. 
        Read break positions and alarm states from J3
        """
        
        ioc = test_inst.IOC

        ioc.start()
        assert ioc.is_running()
        sleep(test_inst.sleepTime)
      
        ## Read U1 breaker position
        pvName = "Pwr-J3U01:CnPw-Q-001:BreakerPosition-RB"
        pv = PV(pvName)
        pvalue = pv.get(timeout=test_inst.getTimeout)
        assert pvalue == 0
        assert pv.severity == 0

        ## Read U7 breaker position (alarm state INVALID since it is not implemented on server)
        pvName = "Pwr-J3U07:CnPw-Q-001:BreakerPosition-RB"
        pv = PV(pvName)
        pvalue = pv.get(timeout=test_inst.getTimeout)
        assert pvalue == 0
        assert pv.severity == 3

        ## Read 3I>>>Trip RB
        pvName = "Pwr-J3U01:CnPw-Q-001:3I--TRIP-RB"
        pv = PV(pvName)
        pvalue = pv.get(timeout=test_inst.getTimeout)
        assert pvalue == 3.117
        assert pv.severity == 1

        ## Read ANY-TRIP for the slot (should be 1 since a trip is active)
        pvName = "Pwr-J3U01:CnPw-Q-001:ANY-TRIP"
        pv = PV(pvName)
        pvalue = pv.get(timeout=test_inst.getTimeout)
        assert pvalue == 1

        ## Read ANY-TRIP for the substation (should be 1 since a trip is active)
        pvName = "Pwr-J3:CnPw-Q-001:ANY-TRIP"
        pv = PV(pvName)
        pvalue = pv.get(timeout=test_inst.getTimeout)
        assert pvalue == 1

        ## Read ANY-TRIP for another substation (should be 0 since a no trip is active)
        pvName = "Pwr-J1:CnPw-Q-001:ANY-TRIP"
        pv = PV(pvName)
        pvalue = pv.get(timeout=test_inst.getTimeout)
        assert pvalue == 0
       
        ioc.exit()
        assert not ioc.is_running()

    def test_UH_003(self, test_inst):
        """
        Start the server, start the IOC. 
        Read break positions from N3
        """
        
        ioc = test_inst.IOC

        ioc.start()
        assert ioc.is_running()
        sleep(test_inst.sleepTime)
 
        ## Read U1 breaker position
        pvName = "Pwr-N3U01:CnPw-Q-001:Breaker-position-RB"
        pv = PV(pvName)
        pvalue = pv.get(timeout=test_inst.getTimeout)
        assert pvalue == 1.0
        assert pv.severity == 0

        ioc.exit()
        assert not ioc.is_running()

    def test_UH_004(self, test_inst):
        """
        Start the server, start the IOC. 
        Check time stamp on breaker position read
        """
        
        ioc = test_inst.IOC

        ioc.start()
        assert ioc.is_running()
        sleep(test_inst.sleepTime)

        ## Read U2 breaker position until it toggles and check that the timestamp matches current time
        pvName = "Pwr-N3U01:CnPw-Q-001:Breaker-position-RB"
        pv = PV(pvName)
        pvalue = pv.get(timeout=test_inst.getTimeout)
        startpv = pvalue
        ## Wait for breaker position toggle
        while startpv == pvalue:
            pvalue = pv.get(timeout=test_inst.getTimeout)
            pstamp = pv.timestamp
        
        ## Get current time
        now = datetime.now()
        timestamp = datetime.timestamp(now)
        
        assert pstamp == pytest.approx(timestamp,abs=0.2)
        print(pstamp)
        print(timestamp)
        print(timestamp-pstamp)
        assert pv.severity == 0
        
        ioc.exit()
        assert not ioc.is_running()

    def test_UH_005(self, test_inst_TZ):
        """
        Start the server with fake timestamping, start the IOC. 
        Verify time stamp
        """
        
        ioc = test_inst_TZ.IOC
        
        ioc.start()
        assert ioc.is_running()
        sleep(test_inst_TZ.sleepTime)

        # Check time stamp (fake time)
        pvName = "Pwr-N3U01:CnPw-Q-001:Breaker-position-RB"
        pv = PV(pvName)
        pvalue = pv.get(timeout=test_inst_TZ.getTimeout)
        pstamp = pv.timestamp
        form = "%Y-%m-%d %H:%M:%S"
        pyTs = datetime.strptime(test_inst_TZ.serverFakeTime, form).timestamp()
        assert pstamp == pyTs, "Timestamp returned does not match"

        ioc.exit()
        assert not ioc.is_running()

    def test_UH_006(self, test_inst):
        """
        Start the server, start the IOC. 
        Stop the server, check for appropriate messaging. 
        Start the server, check that the IOC reconnects.
        """
        ioc = test_inst.IOC

        ioc.start()
        assert ioc.is_running()

        test_inst.stop_server()
        assert ioc.is_running()

        sleep(test_inst.sleepTime)

        ## Check alarm severity, should be "INVALID" since server is down
        pvName = "Pwr-J3U01:CnPw-Q-001:Voltage-RB"
        pv = PV(pvName)
        pvalue = pv.get(timeout=test_inst.getTimeout)
        assert pv.severity == 3

        test_inst.start_server()
        assert ioc.is_running()

        sleep(test_inst.sleepTime)

        ## Check alarm severity, should be "NO_ALARM" since server is up again
        pvName = "Pwr-J3U01:CnPw-Q-001:Voltage-RB"
        pv = PV(pvName)
        pvalue = pv.get(timeout=test_inst.getTimeout)
        assert pv.severity == 0

        ioc.exit()
        assert not ioc.is_running()

    def test_UH_007(self, test_inst):
        """
        Start the server, start the IOC. 
        Attempt to write a PV.
        Verify that nothing is written
        """
        ioc = test_inst.IOC

        ioc.start()
        assert ioc.is_running()
     
        sleep(test_inst.sleepTime)

        ## Attempt to write a PV
        pvName = "Pwr-J3U01:CnPw-Q-001:Voltage-RB"
        pv = PV(pvName)

        ## Read starting value
        startpvalue = pv.get(timeout=test_inst.getTimeout)

        ## Attempt to write to PV
        pv.put(17, wait=True)
                
        ## Wait 1 second
        sleep(1)

        ## Read back PV
        pvalue = pv.get(timeout=test_inst.getTimeout)
        assert pvalue == startpvalue
        
        ioc.exit()
        assert not ioc.is_running()
