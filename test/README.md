# Test - CFHVMS
This directory contains the sources for the automatic testing of the e3-cfhvms module.

## Prerequisites
In order to run the test suite, you must install the following:

 * python3
 * libfaketime

On CentOS 7, run the following:

```
sudo yum install -y python3 libfaketime
```

And the following python modules:

 * pytest
 * pyepics
 * opcua
 * run-iocsh

You can use the following pip3 commands:

```
pip3 install pytest opcua pyepics
pip3 install run-iocsh -i https://artifactory.esss.lu.se/artifactory/api/pypi/pypi-virtual/simple
```

You must configure the EPICS environment before running the test suite. 
For the E3 environment, this requires you to ``source setE3Env.bash``.

Finally, compile the test server for use by the test suite:
```
cd test/server
make
```

## Tests 
The tests for this test scrip are defined in the varification and validation plan for the CFHVMS [1]



## Running the test suite
You can run the test suite from this folder using the following command:
```
pytest -v test_script.py
```

## Expected result
A successful run should look something like this:
```
================================== test session starts ===================================
platform linux -- Python 3.6.8, pytest-6.2.2, py-1.10.0, pluggy-0.13.1 -- /usr/bin/python3
cachedir: .pytest_cache
rootdir: /home/karlvestin/e3/e3-cfhvms/test
collected 7 items                                                                        

test_script.py::TestUnitTests::test_UH_001 PASSED                                  [ 14%]
test_script.py::TestUnitTests::test_UH_002 PASSED                                  [ 28%]
test_script.py::TestUnitTests::test_UH_003 PASSED                                  [ 42%]
test_script.py::TestUnitTests::test_UH_004 PASSED                                  [ 57%]
test_script.py::TestUnitTests::test_UH_005 PASSED                                  [ 71%]
test_script.py::TestUnitTests::test_UH_006 PASSED                                  [ 85%]
test_script.py::TestUnitTests::test_UH_007 PASSED                                  [100%]

=================================== 7 passed in 57.44s ===================================
```

## References
[1] [\[ESS-0145304\]](https://chess.esss.lu.se/enovia/link/ESS-0145304/21308.51166.51456.27357/valid) CF HV Monitoring System - Verification and Validation Plan

