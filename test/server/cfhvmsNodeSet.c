/* WARNING: This is a generated file.
 * Any manual changes will be overwritten. */

#include "cfhvmsNodeSet.h"


/* Simulation - ns=1;s=85/0:Simulation */

static UA_StatusCode function_cfhvmsNodeSet_0_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_ObjectAttributes attr = UA_ObjectAttributes_default;
attr.displayName = UA_LOCALIZEDTEXT("", "Simulation");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for objects that organize other nodes.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_OBJECT,
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 85LU),
UA_NODEID_NUMERIC(ns[0], 35LU),
UA_QUALIFIEDNAME(ns[1], "Simulation"),
UA_NODEID_NUMERIC(ns[0], 61LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_OBJECTATTRIBUTES],NULL, NULL);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_0_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "85/0:Simulation")
);
}

/* 0:\\APL\1\P\J3_U09_LARM\17 - ns=1;s=0:\\APL\1\P\J3_U09_LARM\17 */

static UA_StatusCode function_cfhvmsNodeSet_1_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u09_larm_17_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u09_larm_17_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u09_larm_17_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u09_larm_17_variant_DataContents = (UA_Double) 3.917;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u09_larm_17_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U09_LARM\\17");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U09_LARM\\17"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U09_LARM\\17"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u09_larm_17_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_1_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U09_LARM\\17")
);
}

/* 0:\\APL\1\P\J3_U01_LARM\17 - ns=1;s=0:\\APL\1\P\J3_U01_LARM\17 */

static UA_StatusCode function_cfhvmsNodeSet_2_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u01_larm_17_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u01_larm_17_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u01_larm_17_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u01_larm_17_variant_DataContents = (UA_Double) 3.117;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u01_larm_17_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U01_LARM\\17");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U01_LARM\\17"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U01_LARM\\17"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u01_larm_17_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_2_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U01_LARM\\17")
);
}

/* 0:\\APL\1\P\N3_U02_S\10 - ns=1;s=0:\\APL\1\P\N3_U02_S\10 */

static UA_StatusCode function_cfhvmsNodeSet_3_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_n3_u02_s_10_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_n3_u02_s_10_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_n3_u02_s_10_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_n3_u02_s_10_variant_DataContents = (UA_Double) 3.21;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_n3_u02_s_10_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\N3_U02_S\\10");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\N3_U02_S\\10"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\N3_U02_S\\10"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_n3_u02_s_10_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_3_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\N3_U02_S\\10")
);
}

/* 0:\\APL\1\P\J3_U09_M2\21 - ns=1;s=0:\\APL\1\P\J3_U09_M2\21 */

static UA_StatusCode function_cfhvmsNodeSet_4_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u09_m2_21_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u09_m2_21_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u09_m2_21_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u09_m2_21_variant_DataContents = (UA_Double) 3.9221;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u09_m2_21_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U09_M2\\21");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U09_M2\\21"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U09_M2\\21"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u09_m2_21_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_4_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U09_M2\\21")
);
}

/* 0:\\APL\1\P\J3_U02_LARM\23 - ns=1;s=0:\\APL\1\P\J3_U02_LARM\23 */

static UA_StatusCode function_cfhvmsNodeSet_5_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u02_larm_23_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u02_larm_23_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u02_larm_23_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u02_larm_23_variant_DataContents = (UA_Double) 3.223;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u02_larm_23_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U02_LARM\\23");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U02_LARM\\23"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U02_LARM\\23"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u02_larm_23_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_5_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U02_LARM\\23")
);
}

/* 0:\\APL\1\P\J3_U02_M2\21 - ns=1;s=0:\\APL\1\P\J3_U02_M2\21 */

static UA_StatusCode function_cfhvmsNodeSet_6_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u02_m2_21_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u02_m2_21_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u02_m2_21_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u02_m2_21_variant_DataContents = (UA_Double) 3.2221;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u02_m2_21_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U02_M2\\21");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U02_M2\\21"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U02_M2\\21"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u02_m2_21_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_6_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U02_M2\\21")
);
}

/* 0:\\APL\1\P\J3_U09_LARM\19 - ns=1;s=0:\\APL\1\P\J3_U09_LARM\19 */

static UA_StatusCode function_cfhvmsNodeSet_7_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u09_larm_19_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u09_larm_19_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u09_larm_19_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u09_larm_19_variant_DataContents = (UA_Double) 3.919;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u09_larm_19_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U09_LARM\\19");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U09_LARM\\19"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U09_LARM\\19"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u09_larm_19_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_7_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U09_LARM\\19")
);
}

/* 0:\\APL\1\P\J3_U04_M2\21 - ns=1;s=0:\\APL\1\P\J3_U04_M2\21 */

static UA_StatusCode function_cfhvmsNodeSet_8_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u04_m2_21_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u04_m2_21_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u04_m2_21_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u04_m2_21_variant_DataContents = (UA_Double) 3.4221;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u04_m2_21_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U04_M2\\21");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U04_M2\\21"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U04_M2\\21"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u04_m2_21_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_8_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U04_M2\\21")
);
}

/* 0:\\APL\1\P\J3_U09_LARM\32 - ns=1;s=0:\\APL\1\P\J3_U09_LARM\32 */

static UA_StatusCode function_cfhvmsNodeSet_9_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u09_larm_32_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u09_larm_32_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u09_larm_32_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u09_larm_32_variant_DataContents = (UA_Double) 3.932;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u09_larm_32_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U09_LARM\\32");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U09_LARM\\32"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U09_LARM\\32"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u09_larm_32_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_9_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U09_LARM\\32")
);
}

/* 0:\\APL\1\P\J3_U01_LARM\32 - ns=1;s=0:\\APL\1\P\J3_U01_LARM\32 */

static UA_StatusCode function_cfhvmsNodeSet_10_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u01_larm_32_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u01_larm_32_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u01_larm_32_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u01_larm_32_variant_DataContents = (UA_Double) 3.132;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u01_larm_32_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U01_LARM\\32");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U01_LARM\\32"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U01_LARM\\32"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u01_larm_32_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_10_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U01_LARM\\32")
);
}

/* 0:\\APL\1\P\J1_U01_S\10 - ns=1;s=0:\\APL\1\P\J1_U01_S\10 */

static UA_StatusCode function_cfhvmsNodeSet_11_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j1_u01_s_10_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j1_u01_s_10_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j1_u01_s_10_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j1_u01_s_10_variant_DataContents = (UA_Double) 1.0;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j1_u01_s_10_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J1_U01_S\\10");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J1_U01_S\\10"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J1_U01_S\\10"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j1_u01_s_10_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_11_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J1_U01_S\\10")
);
}

/* 0:\\APL\1\P\J1_U01_M\10 - ns=1;s=0:\\APL\1\P\J1_U01_M\10 */

static UA_StatusCode function_cfhvmsNodeSet_12_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j1_u01_m_10_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j1_u01_m_10_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j1_u01_m_10_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j1_u01_m_10_variant_DataContents = (UA_Double) 1.011;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j1_u01_m_10_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J1_U01_M\\10");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J1_U01_M\\10"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J1_U01_M\\10"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j1_u01_m_10_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_12_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J1_U01_M\\10")
);
}

/* 0:\\APL\1\P\J3_U07_M2\20 - ns=1;s=0:\\APL\1\P\J3_U07_M2\20 */

static UA_StatusCode function_cfhvmsNodeSet_13_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u07_m2_20_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u07_m2_20_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u07_m2_20_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u07_m2_20_variant_DataContents = (UA_Double) 3.722;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u07_m2_20_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U07_M2\\20");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U07_M2\\20"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U07_M2\\20"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u07_m2_20_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_13_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U07_M2\\20")
);
}

/* 0:\\APL\1\P\J3_U01_LARM\23 - ns=1;s=0:\\APL\1\P\J3_U01_LARM\23 */

static UA_StatusCode function_cfhvmsNodeSet_14_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u01_larm_23_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u01_larm_23_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u01_larm_23_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u01_larm_23_variant_DataContents = (UA_Double) 3.123;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u01_larm_23_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U01_LARM\\23");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U01_LARM\\23"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U01_LARM\\23"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u01_larm_23_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_14_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U01_LARM\\23")
);
}

/* 0:\\APL\1\P\J3_U03_M2\20 - ns=1;s=0:\\APL\1\P\J3_U03_M2\20 */

static UA_StatusCode function_cfhvmsNodeSet_15_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u03_m2_20_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u03_m2_20_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u03_m2_20_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u03_m2_20_variant_DataContents = (UA_Double) 3.322;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u03_m2_20_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U03_M2\\20");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U03_M2\\20"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U03_M2\\20"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u03_m2_20_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_15_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U03_M2\\20")
);
}

/* 0:\\APL\1\P\J1_U01_M2\16 - ns=1;s=0:\\APL\1\P\J1_U01_M2\16 */

static UA_StatusCode function_cfhvmsNodeSet_16_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j1_u01_m2_16_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j1_u01_m2_16_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j1_u01_m2_16_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j1_u01_m2_16_variant_DataContents = (UA_Double) 1.01216;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j1_u01_m2_16_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J1_U01_M2\\16");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J1_U01_M2\\16"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J1_U01_M2\\16"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j1_u01_m2_16_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_16_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J1_U01_M2\\16")
);
}

/* 0:\\APL\1\P\J3_U01_LARM\19 - ns=1;s=0:\\APL\1\P\J3_U01_LARM\19 */

static UA_StatusCode function_cfhvmsNodeSet_17_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u01_larm_19_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u01_larm_19_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u01_larm_19_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u01_larm_19_variant_DataContents = (UA_Double) 3.119;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u01_larm_19_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U01_LARM\\19");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U01_LARM\\19"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U01_LARM\\19"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u01_larm_19_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_17_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U01_LARM\\19")
);
}

/* 0:\\APL\1\P\J3_U08_M2\20 - ns=1;s=0:\\APL\1\P\J3_U08_M2\20 */

static UA_StatusCode function_cfhvmsNodeSet_18_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u08_m2_20_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u08_m2_20_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u08_m2_20_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u08_m2_20_variant_DataContents = (UA_Double) 3.822;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u08_m2_20_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U08_M2\\20");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U08_M2\\20"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U08_M2\\20"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u08_m2_20_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_18_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U08_M2\\20")
);
}

/* 0:\\APL\1\P\J3_U01_M2\21 - ns=1;s=0:\\APL\1\P\J3_U01_M2\21 */

static UA_StatusCode function_cfhvmsNodeSet_19_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u01_m2_21_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u01_m2_21_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u01_m2_21_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u01_m2_21_variant_DataContents = (UA_Double) 3.1221;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u01_m2_21_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U01_M2\\21");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U01_M2\\21"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U01_M2\\21"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u01_m2_21_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_19_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U01_M2\\21")
);
}

/* 0:\\APL\1\P\J2_U01_M\12 - ns=1;s=0:\\APL\1\P\J2_U01_M\12 */

static UA_StatusCode function_cfhvmsNodeSet_20_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j2_u01_m_12_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j2_u01_m_12_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j2_u01_m_12_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j2_u01_m_12_variant_DataContents = (UA_Double) 2.0112;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j2_u01_m_12_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J2_U01_M\\12");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J2_U01_M\\12"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J2_U01_M\\12"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j2_u01_m_12_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_20_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J2_U01_M\\12")
);
}

/* 0:\\APL\1\P\J3_U05_M2\20 - ns=1;s=0:\\APL\1\P\J3_U05_M2\20 */

static UA_StatusCode function_cfhvmsNodeSet_21_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u05_m2_20_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u05_m2_20_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u05_m2_20_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u05_m2_20_variant_DataContents = (UA_Double) 3.522;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u05_m2_20_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U05_M2\\20");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U05_M2\\20"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U05_M2\\20"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u05_m2_20_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_21_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U05_M2\\20")
);
}

/* 0:\\APL\1\P\J3_U02_LARM\19 - ns=1;s=0:\\APL\1\P\J3_U02_LARM\19 */

static UA_StatusCode function_cfhvmsNodeSet_22_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u02_larm_19_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u02_larm_19_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u02_larm_19_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u02_larm_19_variant_DataContents = (UA_Double) 3.219;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u02_larm_19_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U02_LARM\\19");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U02_LARM\\19"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U02_LARM\\19"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u02_larm_19_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_22_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U02_LARM\\19")
);
}

/* 0:\\APL\1\P\J3_U06_M2\21 - ns=1;s=0:\\APL\1\P\J3_U06_M2\21 */

static UA_StatusCode function_cfhvmsNodeSet_23_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u06_m2_21_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u06_m2_21_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u06_m2_21_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u06_m2_21_variant_DataContents = (UA_Double) 3.6221;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u06_m2_21_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U06_M2\\21");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U06_M2\\21"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U06_M2\\21"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u06_m2_21_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_23_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U06_M2\\21")
);
}

/* 0:\\APL\1\P\J3_U07_M2\21 - ns=1;s=0:\\APL\1\P\J3_U07_M2\21 */

static UA_StatusCode function_cfhvmsNodeSet_24_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u07_m2_21_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u07_m2_21_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u07_m2_21_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u07_m2_21_variant_DataContents = (UA_Double) 3.7221;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u07_m2_21_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U07_M2\\21");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U07_M2\\21"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U07_M2\\21"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u07_m2_21_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_24_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U07_M2\\21")
);
}

/* 0:\\APL\1\P\J1_U01_M\11 - ns=1;s=0:\\APL\1\P\J1_U01_M\11 */

static UA_StatusCode function_cfhvmsNodeSet_25_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j1_u01_m_11_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j1_u01_m_11_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j1_u01_m_11_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j1_u01_m_11_variant_DataContents = (UA_Double) 1.0111;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j1_u01_m_11_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J1_U01_M\\11");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J1_U01_M\\11"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J1_U01_M\\11"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j1_u01_m_11_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_25_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J1_U01_M\\11")
);
}

/* 0:\\APL\1\P\J2_U01_M\11 - ns=1;s=0:\\APL\1\P\J2_U01_M\11 */

static UA_StatusCode function_cfhvmsNodeSet_26_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j2_u01_m_11_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j2_u01_m_11_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j2_u01_m_11_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j2_u01_m_11_variant_DataContents = (UA_Double) 2.0111;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j2_u01_m_11_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J2_U01_M\\11");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J2_U01_M\\11"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J2_U01_M\\11"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j2_u01_m_11_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_26_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J2_U01_M\\11")
);
}

/* 0:\\APL\1\P\J2_U01_S\10 - ns=1;s=0:\\APL\1\P\J2_U01_S\10 */

static UA_StatusCode function_cfhvmsNodeSet_27_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j2_u01_s_10_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j2_u01_s_10_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j2_u01_s_10_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j2_u01_s_10_variant_DataContents = (UA_Double) 2.0;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j2_u01_s_10_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J2_U01_S\\10");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J2_U01_S\\10"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J2_U01_S\\10"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j2_u01_s_10_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_27_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J2_U01_S\\10")
);
}

/* 0:\\APL\1\P\J2_U01_M\10 - ns=1;s=0:\\APL\1\P\J2_U01_M\10 */

static UA_StatusCode function_cfhvmsNodeSet_28_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j2_u01_m_10_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j2_u01_m_10_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j2_u01_m_10_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j2_u01_m_10_variant_DataContents = (UA_Double) 2.011;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j2_u01_m_10_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J2_U01_M\\10");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J2_U01_M\\10"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J2_U01_M\\10"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j2_u01_m_10_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_28_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J2_U01_M\\10")
);
}

/* 0:\\APL\1\P\J3_U03_M2\16 - ns=1;s=0:\\APL\1\P\J3_U03_M2\16 */

static UA_StatusCode function_cfhvmsNodeSet_29_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u03_m2_16_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u03_m2_16_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u03_m2_16_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u03_m2_16_variant_DataContents = (UA_Double) 3.3216;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u03_m2_16_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U03_M2\\16");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U03_M2\\16"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U03_M2\\16"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u03_m2_16_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_29_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U03_M2\\16")
);
}

/* 0:\\APL\1\P\J3_U04_M2\16 - ns=1;s=0:\\APL\1\P\J3_U04_M2\16 */

static UA_StatusCode function_cfhvmsNodeSet_30_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u04_m2_16_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u04_m2_16_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u04_m2_16_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u04_m2_16_variant_DataContents = (UA_Double) 3.4216;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u04_m2_16_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U04_M2\\16");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U04_M2\\16"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U04_M2\\16"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u04_m2_16_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_30_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U04_M2\\16")
);
}

/* 0:\\APL\1\P\J3_U02_LARM\32 - ns=1;s=0:\\APL\1\P\J3_U02_LARM\32 */

static UA_StatusCode function_cfhvmsNodeSet_31_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u02_larm_32_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u02_larm_32_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u02_larm_32_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u02_larm_32_variant_DataContents = (UA_Double) 3.232;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u02_larm_32_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U02_LARM\\32");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U02_LARM\\32"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U02_LARM\\32"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u02_larm_32_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_31_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U02_LARM\\32")
);
}

/* 0:\\APL\1\P\N3_U01_S\10 - ns=1;s=0:\\APL\1\P\N3_U01_S\10 */

static UA_StatusCode function_cfhvmsNodeSet_32_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_n3_u01_s_10_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_n3_u01_s_10_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_n3_u01_s_10_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_n3_u01_s_10_variant_DataContents = (UA_Double) 3.11;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_n3_u01_s_10_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\N3_U01_S\\10");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\N3_U01_S\\10"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\N3_U01_S\\10"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_n3_u01_s_10_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_32_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\N3_U01_S\\10")
);
}

/* 0:\\APL\1\P\J3_U07_M2\16 - ns=1;s=0:\\APL\1\P\J3_U07_M2\16 */

static UA_StatusCode function_cfhvmsNodeSet_33_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u07_m2_16_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u07_m2_16_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u07_m2_16_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u07_m2_16_variant_DataContents = (UA_Double) 3.7216;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u07_m2_16_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U07_M2\\16");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U07_M2\\16"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U07_M2\\16"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u07_m2_16_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_33_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U07_M2\\16")
);
}

/* 0:\\APL\1\P\J3_U09_LARM\34 - ns=1;s=0:\\APL\1\P\J3_U09_LARM\34 */

static UA_StatusCode function_cfhvmsNodeSet_34_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u09_larm_34_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u09_larm_34_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u09_larm_34_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u09_larm_34_variant_DataContents = (UA_Double) 3.934;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u09_larm_34_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U09_LARM\\34");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U09_LARM\\34"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U09_LARM\\34"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u09_larm_34_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_34_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U09_LARM\\34")
);
}

/* 0:\\APL\1\P\J3_U03_M2\21 - ns=1;s=0:\\APL\1\P\J3_U03_M2\21 */

static UA_StatusCode function_cfhvmsNodeSet_35_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u03_m2_21_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u03_m2_21_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u03_m2_21_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u03_m2_21_variant_DataContents = (UA_Double) 3.3221;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u03_m2_21_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U03_M2\\21");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U03_M2\\21"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U03_M2\\21"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u03_m2_21_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_35_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U03_M2\\21")
);
}

/* 0:\\APL\1\P\J3_U09_M2\20 - ns=1;s=0:\\APL\1\P\J3_U09_M2\20 */

static UA_StatusCode function_cfhvmsNodeSet_36_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u09_m2_20_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u09_m2_20_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u09_m2_20_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u09_m2_20_variant_DataContents = (UA_Double) 3.922;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u09_m2_20_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U09_M2\\20");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U09_M2\\20"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U09_M2\\20"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u09_m2_20_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_36_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U09_M2\\20")
);
}

/* 0:\\APL\1\P\J1_U01_M\12 - ns=1;s=0:\\APL\1\P\J1_U01_M\12 */

static UA_StatusCode function_cfhvmsNodeSet_37_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j1_u01_m_12_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j1_u01_m_12_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j1_u01_m_12_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j1_u01_m_12_variant_DataContents = (UA_Double) 1.0112;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j1_u01_m_12_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J1_U01_M\\12");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J1_U01_M\\12"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J1_U01_M\\12"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j1_u01_m_12_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_37_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J1_U01_M\\12")
);
}

/* 0:\\APL\1\P\J3_U09_LARM\23 - ns=1;s=0:\\APL\1\P\J3_U09_LARM\23 */

static UA_StatusCode function_cfhvmsNodeSet_38_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u09_larm_23_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u09_larm_23_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u09_larm_23_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u09_larm_23_variant_DataContents = (UA_Double) 3.923;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u09_larm_23_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U09_LARM\\23");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U09_LARM\\23"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U09_LARM\\23"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u09_larm_23_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_38_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U09_LARM\\23")
);
}

/* 0:\\APL\1\P\J3_U02_LARM\17 - ns=1;s=0:\\APL\1\P\J3_U02_LARM\17 */

static UA_StatusCode function_cfhvmsNodeSet_39_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u02_larm_17_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u02_larm_17_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u02_larm_17_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u02_larm_17_variant_DataContents = (UA_Double) 3.217;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u02_larm_17_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U02_LARM\\17");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U02_LARM\\17"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U02_LARM\\17"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u02_larm_17_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_39_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U02_LARM\\17")
);
}

/* 0:\\APL\1\P\J3_U02_M2\20 - ns=1;s=0:\\APL\1\P\J3_U02_M2\20 */

static UA_StatusCode function_cfhvmsNodeSet_40_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u02_m2_20_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u02_m2_20_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u02_m2_20_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u02_m2_20_variant_DataContents = (UA_Double) 3.222;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u02_m2_20_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U02_M2\\20");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U02_M2\\20"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U02_M2\\20"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u02_m2_20_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_40_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U02_M2\\20")
);
}

/* 0:\\APL\1\P\J3_U06_M2\20 - ns=1;s=0:\\APL\1\P\J3_U06_M2\20 */

static UA_StatusCode function_cfhvmsNodeSet_41_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u06_m2_20_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u06_m2_20_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u06_m2_20_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u06_m2_20_variant_DataContents = (UA_Double) 3.622;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u06_m2_20_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U06_M2\\20");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U06_M2\\20"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U06_M2\\20"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u06_m2_20_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_41_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U06_M2\\20")
);
}

/* 0:\\APL\1\P\J3_U09_M2\16 - ns=1;s=0:\\APL\1\P\J3_U09_M2\16 */

static UA_StatusCode function_cfhvmsNodeSet_42_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u09_m2_16_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u09_m2_16_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u09_m2_16_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u09_m2_16_variant_DataContents = (UA_Double) 3.9216;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u09_m2_16_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U09_M2\\16");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U09_M2\\16"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U09_M2\\16"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u09_m2_16_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_42_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U09_M2\\16")
);
}

/* 0:\\APL\1\P\J3_U01_LARM\34 - ns=1;s=0:\\APL\1\P\J3_U01_LARM\34 */

static UA_StatusCode function_cfhvmsNodeSet_43_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u01_larm_34_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u01_larm_34_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u01_larm_34_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u01_larm_34_variant_DataContents = (UA_Double) 3.134;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u01_larm_34_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U01_LARM\\34");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U01_LARM\\34"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U01_LARM\\34"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u01_larm_34_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_43_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U01_LARM\\34")
);
}

/* 0:\\APL\1\P\J2_U01_M2\16 - ns=1;s=0:\\APL\1\P\J2_U01_M2\16 */

static UA_StatusCode function_cfhvmsNodeSet_44_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j2_u01_m2_16_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j2_u01_m2_16_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j2_u01_m2_16_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j2_u01_m2_16_variant_DataContents = (UA_Double) 2.1216;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j2_u01_m2_16_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J2_U01_M2\\16");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J2_U01_M2\\16"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J2_U01_M2\\16"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j2_u01_m2_16_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_44_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J2_U01_M2\\16")
);
}

/* 0:\\APL\1\P\J3_U01_M2\20 - ns=1;s=0:\\APL\1\P\J3_U01_M2\20 */

static UA_StatusCode function_cfhvmsNodeSet_45_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u01_m2_20_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u01_m2_20_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u01_m2_20_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u01_m2_20_variant_DataContents = (UA_Double) 3.122;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u01_m2_20_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U01_M2\\20");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U01_M2\\20"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U01_M2\\20"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u01_m2_20_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_45_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U01_M2\\20")
);
}

/* 0:\\APL\1\P\J3_U01_S\10 - ns=1;s=0:\\APL\1\P\J3_U01_S\10 */

static UA_StatusCode function_cfhvmsNodeSet_46_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u01_s_10_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u01_s_10_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u01_s_10_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u01_s_10_variant_DataContents = (UA_Double) 0.0;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u01_s_10_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U01_S\\10");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U01_S\\10"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U01_S\\10"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u01_s_10_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_46_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U01_S\\10")
);
}

/* 0:\\APL\1\P\J3_U05_M2\16 - ns=1;s=0:\\APL\1\P\J3_U05_M2\16 */

static UA_StatusCode function_cfhvmsNodeSet_47_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u05_m2_16_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u05_m2_16_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u05_m2_16_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u05_m2_16_variant_DataContents = (UA_Double) 3.5216;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u05_m2_16_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U05_M2\\16");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U05_M2\\16"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U05_M2\\16"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u05_m2_16_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_47_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U05_M2\\16")
);
}

/* 0:\\APL\1\P\J3_U02_M2\16 - ns=1;s=0:\\APL\1\P\J3_U02_M2\16 */

static UA_StatusCode function_cfhvmsNodeSet_48_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u02_m2_16_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u02_m2_16_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u02_m2_16_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u02_m2_16_variant_DataContents = (UA_Double) 3.2216;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u02_m2_16_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U02_M2\\16");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U02_M2\\16"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U02_M2\\16"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u02_m2_16_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_48_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U02_M2\\16")
);
}

/* 0:\\APL\1\P\J3_U08_M2\16 - ns=1;s=0:\\APL\1\P\J3_U08_M2\16 */

static UA_StatusCode function_cfhvmsNodeSet_49_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u08_m2_16_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u08_m2_16_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u08_m2_16_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u08_m2_16_variant_DataContents = (UA_Double) 3.8216;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u08_m2_16_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U08_M2\\16");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U08_M2\\16"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U08_M2\\16"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u08_m2_16_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_49_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U08_M2\\16")
);
}

/* 0:\\APL\1\P\J3_U04_M2\20 - ns=1;s=0:\\APL\1\P\J3_U04_M2\20 */

static UA_StatusCode function_cfhvmsNodeSet_50_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u04_m2_20_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u04_m2_20_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u04_m2_20_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u04_m2_20_variant_DataContents = (UA_Double) 3.422;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u04_m2_20_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U04_M2\\20");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U04_M2\\20"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U04_M2\\20"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u04_m2_20_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_50_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U04_M2\\20")
);
}

/* 0:\\APL\1\P\J3_U01_M2\16 - ns=1;s=0:\\APL\1\P\J3_U01_M2\16 */

static UA_StatusCode function_cfhvmsNodeSet_51_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u01_m2_16_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u01_m2_16_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u01_m2_16_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u01_m2_16_variant_DataContents = (UA_Double) 3.1216;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u01_m2_16_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U01_M2\\16");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U01_M2\\16"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U01_M2\\16"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u01_m2_16_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_51_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U01_M2\\16")
);
}

/* 0:\\APL\1\P\J3_U02_LARM\34 - ns=1;s=0:\\APL\1\P\J3_U02_LARM\34 */

static UA_StatusCode function_cfhvmsNodeSet_52_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u02_larm_34_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u02_larm_34_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u02_larm_34_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u02_larm_34_variant_DataContents = (UA_Double) 3.234;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u02_larm_34_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U02_LARM\\34");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U02_LARM\\34"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U02_LARM\\34"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u02_larm_34_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_52_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U02_LARM\\34")
);
}

/* 0:\\APL\1\P\J3_U06_M2\16 - ns=1;s=0:\\APL\1\P\J3_U06_M2\16 */

static UA_StatusCode function_cfhvmsNodeSet_53_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u06_m2_16_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u06_m2_16_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u06_m2_16_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u06_m2_16_variant_DataContents = (UA_Double) 3.6216;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u06_m2_16_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U06_M2\\16");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U06_M2\\16"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U06_M2\\16"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u06_m2_16_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_53_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U06_M2\\16")
);
}

/* 0:\\APL\1\P\J3_U05_M2\21 - ns=1;s=0:\\APL\1\P\J3_U05_M2\21 */

static UA_StatusCode function_cfhvmsNodeSet_54_begin(UA_Server *server, UA_UInt16* ns) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
UA_VariableAttributes attr = UA_VariableAttributes_default;
attr.minimumSamplingInterval = -1.000000;
attr.userAccessLevel = 3;
attr.accessLevel = 3;
attr.valueRank = -2;
attr.dataType = UA_NODEID_NUMERIC(ns[0], 11LU);
UA_Double *variablenode_ns_1_s_0_apl_1_p_j3_u05_m2_21_variant_DataContents =  UA_Double_new();
if (!variablenode_ns_1_s_0_apl_1_p_j3_u05_m2_21_variant_DataContents) return UA_STATUSCODE_BADOUTOFMEMORY;
UA_Double_init(variablenode_ns_1_s_0_apl_1_p_j3_u05_m2_21_variant_DataContents);
*variablenode_ns_1_s_0_apl_1_p_j3_u05_m2_21_variant_DataContents = (UA_Double) 3.5221;
UA_Variant_setScalar(&attr.value, variablenode_ns_1_s_0_apl_1_p_j3_u05_m2_21_variant_DataContents, &UA_TYPES[UA_TYPES_DOUBLE]);
attr.displayName = UA_LOCALIZEDTEXT("", "0:\\\\APL\\1\\P\\J3_U05_M2\\21");
#ifdef UA_ENABLE_NODESET_COMPILER_DESCRIPTIONS
attr.description = UA_LOCALIZEDTEXT("", "The type for variable that represents a process value.");
#endif
retVal |= UA_Server_addNode_begin(server, UA_NODECLASS_VARIABLE,
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U05_M2\\21"),
UA_NODEID_STRING(ns[1], "85/0:Simulation"),
UA_NODEID_NUMERIC(ns[0], 47LU),
UA_QUALIFIEDNAME(ns[1], "0:\\\\APL\\1\\P\\J3_U05_M2\\21"),
UA_NODEID_NUMERIC(ns[0], 63LU),
(const UA_NodeAttributes*)&attr, &UA_TYPES[UA_TYPES_VARIABLEATTRIBUTES],NULL, NULL);
UA_Double_delete(variablenode_ns_1_s_0_apl_1_p_j3_u05_m2_21_variant_DataContents);
return retVal;
}

static UA_StatusCode function_cfhvmsNodeSet_54_finish(UA_Server *server, UA_UInt16* ns) {
return UA_Server_addNode_finish(server, 
UA_NODEID_STRING(ns[1], "0:\\\\APL\\1\\P\\J3_U05_M2\\21")
);
}

UA_StatusCode cfhvmsNodeSet(UA_Server *server) {
UA_StatusCode retVal = UA_STATUSCODE_GOOD;
/* Use namespace ids generated by the server */
UA_UInt16 ns[3];
ns[0] = UA_Server_addNamespace(server, "http://opcfoundation.org/UA/");
ns[1] = UA_Server_addNamespace(server, "http://www.prosysopc.com/OPCUA/SimulationNodes");
ns[2] = UA_Server_addNamespace(server, "http://www.prosysopc.com/OPCUA/SampleAddressSpace");

/* Load custom datatype definitions into the server */
bool dummy = (
!(retVal = function_cfhvmsNodeSet_0_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_1_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_2_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_3_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_4_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_5_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_6_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_7_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_8_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_9_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_10_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_11_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_12_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_13_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_14_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_15_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_16_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_17_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_18_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_19_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_20_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_21_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_22_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_23_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_24_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_25_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_26_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_27_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_28_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_29_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_30_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_31_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_32_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_33_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_34_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_35_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_36_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_37_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_38_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_39_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_40_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_41_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_42_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_43_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_44_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_45_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_46_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_47_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_48_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_49_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_50_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_51_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_52_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_53_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_54_begin(server, ns))
&& !(retVal = function_cfhvmsNodeSet_54_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_53_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_52_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_51_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_50_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_49_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_48_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_47_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_46_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_45_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_44_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_43_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_42_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_41_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_40_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_39_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_38_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_37_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_36_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_35_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_34_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_33_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_32_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_31_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_30_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_29_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_28_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_27_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_26_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_25_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_24_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_23_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_22_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_21_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_20_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_19_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_18_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_17_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_16_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_15_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_14_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_13_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_12_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_11_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_10_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_9_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_8_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_7_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_6_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_5_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_4_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_3_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_2_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_1_finish(server, ns))
&& !(retVal = function_cfhvmsNodeSet_0_finish(server, ns))
); (void)(dummy);
return retVal;
}
